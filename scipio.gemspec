# frozen_string_literal: true

# probably no longer required
# lib = File.expand_path("lib", __dir__)
# $LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require_relative "lib/scipio/version"

Gem::Specification.new do |spec|
  spec.name = "scipio"
  spec.version = Scipio::VERSION
  spec.authors = ["Mike Kreuzer"]
  spec.email = ["mike@mikekreuzer.com"]

  spec.summary = "This gem is no longer maintained. Please see mikekreuzer.com for my more recent work."
  spec.description = <<~DESC
    This gem is no longer maintained. Please see mikekreuzer.com for my more recent work.
  DESC
  spec.homepage = "https://codeberg.org/kreuzer/scipio"
  spec.license = "AGPL-3.0-or-later"
  spec.required_ruby_version = ">= 3.3"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata = {
    "changelog_uri" => "#{spec.homepage}/src/branch/main/CHANGELOG.md",
    "homepage_uri" => spec.homepage,
    "rubygems_mfa_required" => "true",
    "source_code_uri" => spec.homepage
  }

  spec.files = Dir.glob("{bin,lib,template}/**/*", File::FNM_DOTMATCH)
    .select { |file| !file.end_with?(*%w[. .. .DS_Store]) } + %w[LICENSE.txt README.md CHANGELOG.md]
  spec.bindir = "bin"
  spec.executables = "scipio"
  spec.require_paths = ["lib"]

  unless ENV.fetch("UNSIGNED_BUILD", "false") == "true"
    spec.cert_chain = ["certs/kreuzer.pem"]
    spec.signing_key = File.expand_path("~/.ssh/gem-private_key.pem")
  end

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
