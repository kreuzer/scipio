# frozen_string_literal: true

require "optparse"
require "yaml"

require_relative "config"
require_relative "gems"
require_relative "git"

module Scipio
  # CLI handled by optparse
  class CLI
    def start(*_args)
      @options = {}
      OptionParser.new do |opts|
        opts.banner = <<~BANNER
          Scipio is a CLI utility to set up Ruby Gems & git repos

          Commands:
        BANNER
        gems(opts:)
        help(opts:)
        init(opts:)
        local(opts:)
        naked_repo(opts:)
        version(opts:)
      end.parse!

      create_gem
      create_repo
    rescue OptionParser::InvalidOption
      puts "Invalid option"
      # don't have opts here to show help
    end

    def gems(opts:)
      opts.on("-g GEM", "--gem GEM", "Create a new ruby gem named GEM") do |opt|
        @options[:gem] = opt
      end
    end

    def help(opts:)
      opts.on("-h", "--help", "This help message") do
        puts opts
        exit
      end
    end

    def init(opts:)
      opts.on("-i", "--initialize", "Initialize config & template files, show details if they exist") do
        config = Scipio::Config.new
        config.info
        gem = Scipio::Gems.new(config:, gem_name: "")
        gem.init
      end
    end

    def local(opts:)
      opts.on("-l", "--local", "With a gem or repo, do not create or connect to the origin") do
        @options[:local] = true
      end
    end

    def naked_repo(opts:)
      opts.on("-n REPO", "--naked REPO", "Create a new naked git repo named REPO") do |opt|
        @options[:repo] = opt
      end
    end

    def version(opts:)
      opts.on("-v", "--version", "The version") do
        puts Scipio::VERSION
        exit
      end
    end

    # TODO: provide a path here to override the default
    def create_gem
      if @options[:gem]
        config = Scipio::Config.new
        repo = Scipio::Git.new(config:, repo_name: @options[:gem])
        gem = Scipio::Gems.new(config:, gem_name: @options[:gem])
        gem.init

        repo.create_dir
        repo.init_before_write
        gem.write
        repo.init_after_write
        unless @options[:local]
          repo.remote
        end

        exit
      end
    end

    def create_repo
      if @options[:repo]
        config = Scipio::Config.new
        repo = Scipio::Git.new(config:, repo_name: @options[:repo])

        repo.create_dir
        repo.naked_write
        unless @options[:local]
          repo.remote
        end

        exit
      end
    end
  end
end
