# frozen_string_literal: true

require "date"
require "erb"
require "fileutils"
require "pathname"

require_relative "config"
require_relative "../stdlib/string"

module Scipio
  # Create the skeleton of a new Gem
  class Gems
    def initialize(config:, gem_name:)
      @gem_default_templates = File.expand_path("../../template", File.dirname(__FILE__))

      @config_dir = Config.config_dir
      @gem_name = gem_name
      @git_dir = config.git_dir
      @template_dir = Config.template_dir
      @vars = {gem_name: @gem_name,
               gem_name_slash: @gem_name.slash,
               gem_name_test: @gem_name.test,
               git_site: config.git_site,
               user_name: config.user_name,
               user_fullname: config.user_fullname,
               user_email: config.user_email,
               date: DateTime.now}
    end

    def init
      FileUtils.mkdir_p(@template_dir)
      if Dir.empty?(@template_dir)
        FileUtils.cp_r(@gem_default_templates, @config_dir)
        puts "Templates copied"
      end
      puts "Templates directory - #{@template_dir}"
    end

    def relative_out_path(path_from:)
      out = Pathname.new(path_from)
        .relative_path_from(@template_dir)
        .to_s
        .chomp(".erb")
      out % @vars
    end

    def templates
      # except for dot files this could be... Dir[File.join(dir, "**/*")]
      filter_types = %w[. .. .DS_Store]
      Dir.glob(File.join(@template_dir, "**/*"), File::FNM_DOTMATCH)
        .select { |file| !file.end_with?(*filter_types) }
    end

    def write
      out_dir = File.join(@git_dir, @gem_name)
      # create_out_dir(out_dir:)
      templates.each do |path_from|
        path_to = File.join(out_dir, relative_out_path(path_from:))

        if File.directory? path_from
          FileUtils.mkdir_p path_to
        else
          puts path_to # TODO: unless silent
          FileUtils.mkdir_p Pathname(path_to).dirname # just in case
          template = ERB.new(File.read(path_from))
          IO.write(path_to, template.result_with_hash(@vars))
        end
      end
    end

    # copy the default template files for users to modify or use
  end
end
