# frozen_string_literal: true

module Scipio
  class Git
    def initialize(config:, repo_name:)
      @git_domain = config.git_domain
      @repo_name = repo_name
      @user_name = config.user_name

      @dir = File.join(config.git_dir, @repo_name)
      # create_dir
      ensure_main
    end

    def create_dir
      if File.exist?(@dir)
        puts "Directory already exists - #{@dir}"
        exit
      end
      FileUtils.mkdir_p(@dir)
    end

    # TODO: call out changing git setup, and/or push name to config
    def ensure_main
      system "git config --global init.defaultBranch main"
    end

    def init_before_write
      Dir.chdir(@dir) do
        system "git init"
      end
    end

    # alternative to writing out gem files
    def init_write
      Dir.chdir(@dir) do
        system "touch README.md"
        system "touch .gitignore"
      end
    end

    def init_after_write
      Dir.chdir(@dir) do
        system "git checkout -b main"
        system "git add ."
        system 'git commit -m "Initial commit"'
      end
    end

    def naked_write
      init_before_write
      init_write
      init_after_write
    end

    # TODO - alt for github, etc
    # TODO - ssh/sftp choice
    #
    # fatal: protocol 'git@https' is not supported
    def remote
      Dir.chdir(@dir) do
        system "berg repo create --default-branch main --description 'To follow' --name #{@repo_name} --private public"
        system "git remote add origin git@#{@git_domain}:#{@user_name}/#{@repo_name}.git"
        system "git push -u origin main"
      end
    end
  end
end
