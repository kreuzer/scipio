# frozen_string_literal: true

require "yaml"

module Scipio
  # Persist & use configuration
  class Config
    attr_reader :git_dir,
      :git_domain,
      :git_site,
      :user_email,
      :user_fullname,
      :user_name

    def self.config_dir
      File.join(Config.home_dir, ".config/scipio")
    end

    def self.config_file
      File.join(Config.config_dir, "config.yml")
    end

    def self.home_dir
      path = if RUBY_PLATFORM.match?(/cygwin | mswin | mingw | bccwin | wince | emx /)
        "%userprofile%"
      else
        "~"
      end
      File.expand_path(path)
    end

    def self.template_dir
      File.join(Config.config_dir, "template")
    end

    def initialize
      if File.exist?(Config.config_file)
        read_file
      else
        init
      end
    end

    # TODO errors
    # TODO read config values as defaults, with chance to overide interactively
    #  rescue Errno::ENOENT => _e
    #    puts "No config file present"
    #  rescue Psych::SyntaxError => error
    #    puts "Could not parse config: #{error.message}"
    #  ensure
    #    exit
    #  end

    def init
      if File.exist?(Config.config_file)
        return
      end
      ask_for_git_dir
      parse_git_config
      parse_berg_user_info
      parse_berg_config
      @git_site = File.join(@git_site, @user_name)
      FileUtils.mkdir_p(Config.template_dir)
      save_file
    end

    def info
      puts <<~INFO
        Current config in - #{Config.config_file}

        Git
        local: \t#{@git_dir}
        domain: #{@git_domain}
        origin: #{@git_site}
        e-mail: #{@user_email}
        name: \t#{@user_fullname}
        user: \t#{@user_name}
      INFO
    end

    def ask_for_git_dir
      puts "What directory should local git repos be created in?"
      dir = File.expand_path($stdin.gets.chomp.strip)
      raise StandardError.new "#{dir} - doesn't seem to exists, enter manually in file" unless File.directory?(dir)
    rescue => error
      puts error.message
      dir = ""
    ensure
      @git_dir = dir
    end

    def read_file
      config = YAML.safe_load_file(Config.config_file, permitted_classes: [Hash])
      git = config["git"]
      @git_dir = git["git_dir"]
      @git_domain = git["git_domain"]
      @git_site = git["git_site"]
      @user_email = git["user_email"]
      @user_fullname = git["user_fullname"]
      @user_name = git["user_name"]
    end

    def save_file
      config = {"git" => {
                  "git_dir" => @git_dir,
                  "git_domain" => @git_domain,
                  "git_site" => @git_site,
                  "user_email" => @user_email,
                  "user_fullname" => @user_fullname,
                  "user_name" => @user_name
                }}
      yaml = config.to_yaml
      File.write(Config.config_file, yaml)
    end

    # ~/.Config -> @user_email, @user_fullname
    def parse_git_config
      git_config = File.join(Config.home_dir, ".gitconfig")
      File.readlines(git_config, chomp: true).each do |line|
        res = line.match(/email = (?<email>\S+)/)
        unless res.nil?
          @user_email = res[:email]
        end
        res = line.match(/name = (?<name>.+)$/)
        unless res.nil?
          @user_fullname = res[:name]
        end
      end
    end

    # berg user info -> @user_name
    def parse_berg_user_info
      bash = `berg user info`
      res = bash.match(/│ Username\s+┆ (?<name>\S+)\s+│/)
      @user_name = res[:name]
      # TODO: could not get git user name from berg, please enter manually - #{Config.config_file}
    end

    # berg config info -> @git_site
    def parse_berg_config
      # ~/Library/Application Support/berg-cli/berg.toml -> git_site
      # but would be OS specific
      # berg_config = File.join(home_dir, "Library/Application Support/berg-cli/berg.toml")
      # url = ""
      # protocol = ""
      # File.readlines(berg_config, chomp: true).each do |line|
      #   res = line.match(/base_url = (?<url>\S+)/)
      #   unless res.nil?
      #     url = res[:url].tr("\"", "")
      #   end
      #   res = line.match(/protocol = (?<protocol>.+)$/)
      #   unless res.nil?
      #     protocol = res[:protocol].tr("\"", "")
      #   end
      # end
      # @git_site = "#{protocol}://#{url}"
      bash = `berg config info`
      protocol = bash.match(/│ protocol\s+┆ ".+" ┆ (?<protocol>\S+)\s+│/)
      url = bash.match(/│ base_url\s+┆ ".+" ┆ (?<url>\S+)\s+│/)
      @git_domain = url[:url].tr("\"", "").tr("/", "")
      @git_site = "#{protocol[:protocol].tr("\"", "")}://#{@git_domain}"
    end
  end
end
