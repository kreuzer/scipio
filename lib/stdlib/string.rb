# frozen_string_literal: true

# Extending the String class just like I promised I'd never do
class String
  # these_words to TheseWords, this to This
  def camelize
    split("_").map { |word| word.capitalize_first }.join
  end

  # worD to WordD (& not Word, which is what capitalize would do)
  def capitalize_first
    sub(/\w/) { |word| word.upcase }
  end

  # these-words to These::Words - used for Module names
  def colonize
    # sub("-", "::")
    split("-").map { |word| word.capitalize_first }.join("::")
  end

  # this-and-that to that - used in require_relative
  def last_part
    File.basename(tr("-", "/"))
  end

  # these-words to these/words - used in file paths
  def slash
    tr("-", "/")
  end

  # these-words to these/test_words
  def test
    slashed = slash
    last = slashed.last_part
    slashed.sub(/.*\K#{last}/, "test_#{last}")
  end
end
