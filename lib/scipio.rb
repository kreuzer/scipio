# frozen_string_literal: true

require_relative "scipio/version"

# Scipio's top level module
module Scipio
end
