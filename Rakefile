# frozen_string_literal: true

require "digest/sha2"
require "minitest/test_task"
require "standard/rake"

require_relative "lib/scipio/version"

Minitest::TestTask.create

# desc "Calculate checksum - for display in the repo, not the pkg file"
# task :checksum do
#  check
# end

namespace :build do
  def check
    built_gem_path = "pkg/scipio-#{Scipio::VERSION}.gem"
    checksum = Digest::SHA512.new.hexdigest(File.read(built_gem_path))
    checksum_path = "./checksums/scipio-#{Scipio::VERSION}.gem.sha512"
    FileUtils.rm(Dir.glob("./checksums/*"))
    File.write(checksum_path, checksum)
  end

  def signed_build
    ENV["UNSIGNED_BUILD"] = "false"
    sh "bundle exec gem build scipio.gemspec --output=./pkg/scipio-#{Scipio::VERSION}.gem"
  end

  desc "Unsigned build & local install"
  task :local do
    ENV["UNSIGNED_BUILD"] = "true"
    sh "bundle exec gem build scipio.gemspec --output=./pkg/scipio-#{Scipio::VERSION}.gem"
    sh "bundle exec gem install ./pkg/scipio-#{Scipio::VERSION}.gem -P MediumSecurity"
    check
  end

  desc "Signed build & local install"
  task :local_sy do
    signed_build
    check
    sh "bundle exec gem install ./pkg/scipio-#{Scipio::VERSION}.gem -P HighSecurity"
  end

  desc "Publish it - this will build & sign, then commit, push and publish"
  task :publish do
    # signed build & checksum update
    signed_build
    check
    # tag git repo
    sh "git add ."
    sh "git commit -m \"v#{Scipio::VERSION}\""
    sh "git tag -s v#{Scipio::VERSION} -m \"v#{Scipio::VERSION}\""
    sh "git push origin \"v#{Scipio::VERSION}\""
    sh "git push"
    # publish to rubygems, will require OTP
    sh "gem push ./pkg/scipio-#{Scipio::VERSION}.gem"
  end
end

# desc "Set bundle security policy to MediumSecurity (unsigned dependencies)"
# task :sy_bundle do
#  sh "bundle --trust-policy MediumSecurity"
# end

task default: %i[standard:fix reek]
