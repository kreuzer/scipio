# Scipio Changelog

## 0.8.0 - 28 August 2024

- Breaking changes
  - split off Rake tasks to the new Reki gem, to focus on new gems & repos here
  - minor template changes (named arguments in the cli methods)
- bug fix - attempt gem creation with no local template copy present

## 0.7.0 - 27 August 2024

- Breaking changes
  - bug fix/add support for - hyphens & underscores in gem names
  - add .erb extensions to the default template files
  - add RBS files to the default template
- Add RBS & type signatures
- Some refactoring

## 0.6.0 - 13 August 2024

- Initial Ruby release (earlier versions were a Python project)
